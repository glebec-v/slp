<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
          function __autoload($class_name) {
            include 'equipment/'. $class_name . '.php';
          }
          
          // конфигурим вентмашину:
          $heat = new Heater();
          $cool = new Cooler();
          $fann = new Fan();
          $term = new Termostat();
          $f1   = new Filter();
          $f2   = new Filter();
          $b    = new BaseMachine('П1');
          
          $ventmash = new VMachine($fann, $heat, $cool, $term, $f1, $f2, $b);
          $vm_vars = get_object_vars($ventmash);
          
          // выводим статус машины
          /*
          foreach ($vm_vars as $key) {
              $k = explode(':', serialize($key));
              echo $k[2].'<br/>'; // костыль...выводим название переменной объекта вентмашины (ее составная часть)
              foreach ($key as $name => $value) {
                  echo "$name : $value".'<br/>';
              }
              echo '<hr>';
          }
          // ------ меняем значения вложенного обьекта
          echo '<hr>';
          $ventmash->fan->setFanSpeed(200);
          foreach ($ventmash->fan->getStatus() as $key => $value) {
              echo "$key : $value".'<br/>';
          }
           * 
           */
          // ------ доступ к переменным класса из класса
          echo '<hr>';
          $ventmash->summerStart();
          //$ventmash->ShowStatus();
          
          $ventmash->Failure(true, 'fan');
          $ventmash->ShowStatus();
          //$ventmash->fanFailure(false);
          //$ventmash->ShowStatus();
          
        ?>
    </body>
</html>
