<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VMachine
 *
 * @author glebec
 */
class VMachine {
  function __construct( 
          Fan $f, 
          Heater $h, 
          Cooler $c, 
          Termostat $t,
          Filter $f1,
          Filter $f2,
          BaseMachine $base
          )
  {
    $this->fan = $f;
    $this->heater = $h;
    $this->cooler = $c;
    $this->termostat = $t;
    $this->f1 = $f1;
    $this->f2 = $f2;
    $this->base = $base;
  }
  function ShowStatus() // сохраняет статус установки в лог-файл
  {
      // берем статус всех переменных модулей машины
      $parts = get_class_vars('VMachine');
      foreach( $parts as $name => $value){
           $status = $this->$name->getStatus();
           foreach($status as $key => $value){
               echo "$key : $value".'<br/>';
           }
      }
  }
  function Failure($on, $device){
      if($on) $this->$device->setFailure();
      else $this->$device->fixFailure();
  }
  
  function summerStart() // ПУСК-ЛЕТО
  {
      $parts = get_class_vars('VMachine');
      foreach( $parts as $name => $value){
        $status = $this->$name->turnSummer();
        $status = $this->$name->setOn();
      }
  }
  function summerStop()  // СТОП-ЛЕТО
  {
    $parts = get_class_vars('VMachine');
      foreach( $parts as $name => $value){
        $status = $this->$name->turnSummer();
        $status = $this->$name->setOff();
      }  
  }
  function winterStart() // ПУСК-ЗИМА
  {
    $parts = get_class_vars('VMachine');
      foreach( $parts as $name => $value){
        $status = $this->$name->turnWinter();
        $status = $this->$name->setOn();
      }  
  }
  function winterStop()  // СТОП-ЗИМА
  {
    $parts = get_class_vars('VMachine');
      foreach( $parts as $name => $value){
        $status = $this->$name->turnWinter();
        $status = $this->$name->setOff();
      }  
  }
  
  public $fan;
  public $cooler;
  public $heater;
  public $termostat;
  public $f1;
  public $f2;
  public $base;
  
}
