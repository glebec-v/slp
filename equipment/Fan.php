<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fan
 *
 * @author glebec
 */
class Fan extends FailureOperations implements ModuleOperations {
  
  function __construct() {
      $this->fun = 'картинка НЕ ВРАЩАЕТСЯ';
      $this->vel_ind = 0;     // при создании вентилятор не крутится
      //$this->_fail_status = '';
  }
  function setOff() {
      $this->setFanSpeed(0);
  }
  function setOn() {
      $this->setFanSpeed(self::DEFSPEED);
  }
  function setFanSpeed( $speed ) {
      $this->vel_ind = $speed;
      if ($speed !== 0) {
        $this->fun = 'картинка ВРАЩАЕТСЯ';
      }else{
        $this->fun = 'картинка НЕ ВРАЩАЕТСЯ';
      }
  }
  function turnSummer() {
      return true;
  }
  function turnWinter() {
      return true;
  }
  function getStatus(){
      $status = [];
      $status['vel_ind'] = $this->vel_ind;
      $status['fun'] = $this->fun;
      return $status;
  }
  function setFailure(){
      $this->setFStatus('АВАРИЯ ВЕНТИЛЯТОРА');
      $this->setOff();
  }
  function fixFailure() {
       $this->setFStatus('');
       $this->setOn();
  }
  
  public $fun; // (4,24) индикатор работы вентилятора
  public $vel_ind;  // (20) числовой индикатор выбранной скорости вращения
  const DEFSPEED = 5;
}
