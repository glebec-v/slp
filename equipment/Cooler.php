<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cooler
 *
 * @author glebec
 */
class Cooler implements ModuleOperations {
  function __construct(){
    $this->cool_ind = 'черная змейка';
    $this->koolsys_ind = 'рисуем бледную систему голубых труб';
    $this->ps2 = 'НОРМА';
    $this->cold3x = 'ОТКР';
    $this->coldflow_ind = 'картинка НЕТ ПОТОКА';
  }
  
  function getStatus(){
    $status = [];
    $status['cool_ind'] = $this->cool_ind;
    $status['koolsys_ind'] = $this->koolsys_ind;
    $status['ps2'] = $this->ps2;
    $status['cold3x'] = $this->cold3x;
    $status['coldflow_ind'] = $this->coldflow_ind;
    return $status;
  }
  function turnSummer() {
      $this->setInternalOn();
  }
  function turnWinter() {
      $this->setInternalOff();
  }
  function setOn() {
    return true;
  }
  function setOff(){
    return true;
  }
 
  // используется в летних режимах
  private function setInternalOn(){
    $this->cool_ind = 'голубая змейка';
    $this->koolsys_ind = 'рисуем систему голубых труб';
    $this->ps2 = 'НОРМА';
    $this->cold3x = 'ОТКР';
    $this->coldflow_ind = 'картинка синие стрелочки';  
    return true;
  }
  
  private function setInternalOff(){
    $this->cool_ind = 'черная змейка';
    $this->koolsys_ind = 'рисуем бледную систему голубых труб';
    $this->ps2 = 'НОРМА';
    $this->cold3x = 'ОТКР';
    $this->coldflow_ind = 'картинка НЕТ ПОТОКА'; 
    return true;
  }
  
  function render(){}     // рисуем блок охлаждения
  
  public $cool_ind;      // (23) индикатор работы охлаждения
  public $koolsys_ind;   // (34) индикатор работы системы охлаждения (дубляж?)
  public $ps2;           // (29) индикатор датчика давления
  public $cold3x;        // (32) индикатор 3-ходового клапана
  public $coldflow_ind;  // (36) индикатор холодного потока
  
  private $isWinter;
}
