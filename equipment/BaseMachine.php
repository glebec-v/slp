<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseMachine
 *
 * @author glebec
 */
class BaseMachine extends FailureOperations implements ModuleOperations {
    
    public $name;        // (1) название установки
    public $sstate_1;    // (2) статус ВУ
    public $ss_1;        // (3) переключатель ПУСК/СТОП
    public $inp_t;       // (5) поле ввода времени запуска и остановки ВУ по расписанию
    public $btn_t;       // (6) кнопка активации запуска ВУ по таймеру
    public $ws_1;        // (7) переключатель режима работы ЛЕТО/ЗИМА
    public $ws_ind;      // (8) индикатор режима работы ЛЕТО/ЗИМА
    public $setT1;       // (11) ползунок выбора значений уставки
    public $set_ind;     // (12) числовой индикатор выбранной температуры уставки
    public $te2;         // (13) числовой индикатор температуры в канале
    public $te1;         // (15) числовой индикатор наружной температуры
    public $valve_ind;   // (16) индикатор состояния заслонки
    public $inc_ind;     // (21) индикатор открытой заслонки и входящего потока
    public $out_ind;     // (25) индикатор горячего потока на выходе 
    public $inc_num;     // (26) числовой индикатор температуры входящего потока
    
    function __construct( $name ) {
        $this->name = $name;
        $this->sstate_1 = 'СИСТЕМА В ЖОПЕ';
        $this->ss_1 = false;                  // true-ПУСК, false-СТОП
        $this->inp_t = 0;                     //
        $this->btn_t = 'АКТИВИРОВАТЬ';        // другое значение -ОТКЛЮЧИТЬ
        $this->ws_1 = 'ЗИМА';                 // другое значение -ЛЕТО
        $this->setT1 = 'left';
        $this->set_ind = 0;
        mt_srand();
        $this->te2 = $this->set_ind + mt_rand(0,1);
        mt_srand();
        $this->te1 = mt_rand(0,30)*(-1);      // случайная наружная температура (-30,,0)
        $this->valve_ind = 'ЗАКР';
        $this->inc_ind = 'рисунок ЗАКРЫТО';
        $this->out_ind = 'рисунок НЕТ НИЧЕГО';
        $this->inc_num = $this->te1;
    }
    
    function setFailure() {
        $this->setFStatus('АВАРИЯ ЗАСЛОНКИ');
        $this->sstate_1 = self::_fail_status;
        $this->setOff();
    }
    function fixFailure() {
        $this->_fail_status = '';
        $this->sstate_1 = 'СИСТЕМА В НОРМЕ';
    }
            
    function setOn(){
        $this->sstate_1 = 'СИСТЕМА В ЖОПЕ и НЕ работает';
        $this->ss_1 = true;                   // true-ПУСК, false-СТОП
        $this->inp_t = 0;                     //
        $this->btn_t = 'АКТИВИРОВАТЬ';        // другое значение -ОТКЛЮЧИТЬ
        mt_srand();
        $this->te2 = $this->set_ind + mt_rand(0,1);
        mt_srand();
        $this->valve_ind = 'ОТКР';
        $this->inc_ind = 'рисунок ОТКРЫТО';
        $this->out_ind = 'рисунок красные стрелочки';
        $this->inc_num = $this->te1;
        return true;
    }
    
    function turnWinter(){
        $this->ws_1 = 'ЗИМА'; 
        $this->te1 = mt_rand(0,30)*(-1);      // случайная зимняя наружная температура (-30,,0)
        return true;
    }
    
    function turnSummer(){
        $this->ws_1 = 'ЛЕТО';
        $this->te1 = mt_rand(10,40);          // случайная летняя наружная температура (10,,40)
        return true;
    }
    
    function setOff(){
        $this->sstate_1 = 'СИСТЕМА В НОРМЕ';
        $this->ss_1 = false;                   // true-ПУСК, false-СТОП
        $this->inp_t = 0;                     //
        $this->btn_t = 'АКТИВИРОВАТЬ';        // другое значение -ОТКЛЮЧИТЬ
        mt_srand();
        $this->te2 = $this->set_ind + mt_rand(0,1);
        mt_srand();
        $this->valve_ind = 'ЗАКР';
        $this->inc_ind = 'рисунок ЗАКРЫТО';
        $this->out_ind = 'рисунок НЕТ НИЧЕГО';
        $this->inc_num = $this->te1;
        return true;
    }
    
    function getStatus(){
        if($this->getFStatus() == '') $this->sstate_1 = 'СИСТЕМА В НОРМЕ';
        else $this->sstate_1 = $this->getFStatus();
        $status = [];
        $status['name'] = $this->name;
        $status['sstate_1'] = $this->sstate_1;
        $status['ss_1'] = $this->ss_1;
        $status['inp_t'] = $this->inp_t;
        $status['btn_t'] = $this->btn_t;
        $status['ws_1'] = $this->ws_1;
        $status['ws_ind'] = $this->ws_ind;
        $status['setT1'] = $this->setT1;
        $status['set_ind'] = $this->set_ind;
        $status['te2'] = $this->te2;
        $status['te1'] = $this->te1;
        $status['valve_ind'] = $this->valve_ind;
        $status['inc_ind'] = $this->inc_ind;
        $status['out_ind'] = $this->out_ind;
        $status['inc_num'] = $this->inc_num;
        return $status;
    }
}
