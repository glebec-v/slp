<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Termostat
 *
 * @author glebec
 */
class Termostat implements ModuleOperations {
    public $therm_ind; // (19) индикатор состояния термостата
    public $failure;
    
    function __construct() {
        $this->therm_ind = 'NORMA';
        $this->failure = false;
    }
    function Failure() {
        $this->failure = true;
        $this->therm_ind = 'FREEZED';
    }
    function Restore() {
        $this->therm_ind = 'NORMA';
        $this->failure = false;
    }
    function getStatus(){
        $status = [];
        $status['therm_ind'] = $this->therm_ind;
        return $status;
    }
    function setOn() {
        if(!$this->failure) {
            $this->therm_ind = 'NORMA';
            return true;
        }
        else return false;
    }
    function setOff(){
        return true;
    }
    function turnSummer() {
      return true;
    }
    function turnWinter() {
      return true;
    }
}
