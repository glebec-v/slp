<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Heater
 *
 * @author glebec
 */
class Heater implements ModuleOperations{
  
  // по умолчанию режим работы СТОП-ЗИМА
  function __construct(){
    $this->warm_ind = 'рисуем красную змейку';
    $this->hotflow_ind = 'рисуенм красные стрелочки';
    $this->warmsys_ind = 'рисуем красную систему труб';
    $this->ps1 = 'НОРМА';           
    $this->pump_ind = 'ВКЛ';      
    $this->waterback_ind = 60; 
    $this->hott3x = 'ОТКР';
    $this->isWinter = true;
  }
  
  function getStatus(){
    $status = [];
    $status['warm_ind'] = $this->warm_ind;
    $status['hotflow_ind'] = $this->hotflow_ind;
    $status['warmsys_ind'] = $this->warmsys_ind;
    $status['ps1'] = $this->ps1;           
    $status['pump_ind'] = $this->pump_ind;      
    $status['waterback_ind'] = $this->waterback_ind; 
    $status['hott3x'] = $this->hott3x;
    return $status;
  }
  function turnSummer() {
      $this->setInternalOff();
  }
  function turnWinter() {
      $this->setInternalOn();
  }
  
  function setOn() {
    return true;
  }
  function setOff(){
    return true;
  }
  
  // используется в зимних режимах
  private function setInternalOn(){
    $this->warm_ind = 'рисуем красную змейку';
    $this->hotflow_ind = 'рисуем красные стрелочки';
    $this->warmsys_ind = 'рисуем красную систему труб';
    $this->ps1 = 'НОРМА';           
    $this->pump_ind = 'ВКЛ';      
    $this->waterback_ind = 60; 
    $this->hott3x = 'ОТКР';
    return true;
  }
  
  // используется в летних режимах
  private function setInternalOff(){
    $this->warm_ind = 'рисуем черную змейку';
    $this->hotflow_ind = 'рисуем НЕТ СТРЕЛОЧЕК';
    $this->warmsys_ind = 'рисуем прозрачно-красную систему труб';
    $this->ps1 = 'НОРМА';           
    $this->pump_ind = 'ВЫКЛ';      
    $this->waterback_ind = 25; 
    $this->hott3x = 'ЗАКР'; // в ТЗ ничего нет, догадка
    return true;
  }
  
  // пока не понятно как использовать
  function render(){}     // рисуем блок нагрева
  
  
  public $warm_ind;      // (22) индикатор работы нагрева
  public $warmsys_ind;   // (33) индикатор работы системы нагрева (дубляж?)
  public $ps1;           // (28) индикатор датчика давления
  public $pump_ind;      // (27) индикатор состояния насоса
  public $waterback_ind; // (14,30) числовой индикатор температуры обратной воды (пост знач из табл)
  public $hott3x;        // (31) индикатор 3-ходового клапана
  public $hotflow_ind;   // (35) индикатор горячего потока
  
  private $isWinter;       // внутренняя переменная, хранит текущий сезон  
}
