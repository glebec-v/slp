<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FailureOperations
 *
 * @author glebec
 */
abstract class FailureOperations {
    public static $_fail_status = '';
    
    function setFStatus($status){
        self::$_fail_status = $status;
    }
    function getFStatus(){
        return self::$_fail_status;
    }
    abstract function setFailure();
    abstract function fixFailure();
}
