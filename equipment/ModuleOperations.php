<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author glebec
 */
interface ModuleOperations {
    //описываем шину подключения для всех модулей вентмашины
    function getStatus();
    function setOn();
    function setOff();
    function turnWinter();
    function turnSummer();
}
